from django.db import models


class User(models.Model):
    username = models.CharField(max_length=50)
    
    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return '%s' % self.username


class RequestFriends(models.Model):
    sender = models.ForeignKey(User, models.DO_NOTHING, related_name="sender")
    taker = models.ForeignKey(User, models.DO_NOTHING, related_name="taker")
    accept = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Запрос в друзья"
        verbose_name_plural = "Запросы в друзья"
        
    def __str__(self):
        return '%s - %s' % (self.sender, self.taker)