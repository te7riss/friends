from django.contrib import admin
from .models import *


class RequestFriendsInline(admin.TabularInline):
    model = RequestFriends
    fk_name = "sender"


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    inlines = [
        RequestFriendsInline,
    ]