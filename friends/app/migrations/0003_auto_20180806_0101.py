# Generated by Django 2.1 on 2018-08-05 22:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20180806_0100'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='requestfriends',
            options={'verbose_name': 'Запрос в друзья', 'verbose_name_plural': 'Запросы в друзья'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'Пользователь', 'verbose_name_plural': 'Пользователи'},
        ),
    ]
