from .models import *


# Put that method to CBV get_context method or some other place
def get_friends(user_id):
    friends = RequestFriends.objects.filter(
        sender=User.objects.get(id=user_id),
        accept=True
    )
    return friends


# Some magic here